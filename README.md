# Run it

Install Ansible:
```sh
sudo apt install pipx
pipx install --include-deps ansible
# TODO: can I auto-install galaxy collections?
```

Run from this repo:
```sh
ansible-pull --ask-become-pass -U https://gitlab.com/petitjam_configs/home-ansible
```

Run locally:
```sh
ansible-playbook -i hosts -l localhost --ask-become-pass local.yml
```

# TODO

Cursor
```
.Xresources
~/.local/share/icons/Mocu-* cursor themes
```

Fix warning on playbook start about python interpreter.
