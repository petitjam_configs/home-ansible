export EDITOR=nvim

if [[ :$PATH: != *:$HOME/bin/:* ]]; then
    export PATH="$HOME/bin:$PATH"
fi

if [[ :$PATH: != *:$HOME/.local/bin/:* ]]; then
    export PATH="$HOME/.local/bin:$PATH"
fi

# rbenv
if [[ :$PATH: != *:$HOME/.rbenv/bin/:* ]]; then
    export PATH="$PATH:$HOME/.rbenv/bin/"
    eval "$(~/.rbenv/bin/rbenv init - bash)"
fi

# rust/cargo
[ -f $HOME/.cargo/env ] && source $HOME/.cargo/env

# mise
if ! type "mise" > /dev/null; then
    eval "$(mise activate)"
fi

if [[ :$PATH: != *:$HOME/.local/share/mise/shims:* ]]; then
    export PATH="$HOME/.local/share/mise/shims:$PATH"
fi
