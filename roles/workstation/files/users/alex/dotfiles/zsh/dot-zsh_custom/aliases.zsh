alias zshconfig="nvim ~/.zshrc"
alias ohmyzsh="nvim ~/.oh-my-zsh"

# unbind omz defaults
bindkey -r '\el'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias sudo='sudo '

alias resrc='source ~/.bashrc'

# TODO: install completions for zsh/omz
alias mux='tmuxinator'
complete -F _tmuxinator mux

alias v='nvim '
alias vim='echo "nvim?"'

# TODO: install completions for zsh/omz
alias g='git '
#[ -f /usr/share/bash-completion/completions/git ] && . /usr/share/bash-completion/completions/git
#__git_complete g __git_main

# i3
steamworkspace() {
    i3-msg 'workspace 3:   Steam; append_layout ~/.config/i3/steam-workspace.json'
}

prettypath() {
    echo -e ${PATH//:/\\n}
}

fontshowcase() {
    echo -e \
           "\n  normal \n" \
         "\e[1m bold \e[0m\n" \
         "\e[3m italic \e[0m\n" \
    "\e[3m\e[1m bold italic \e[0m\n" \
         "\e[2m dim \e[0m\n" \
       "\e[1;2m dim bold \e[0m\n" \
       "\e[2;3m dim italic \e[0m\n" \
     "\e[1;2;3m dim bold italic \e[0m\n" \
         "\e[4m underline \e[0m\n" \
         "\e[9m strikethrough \e[0m\n" \
        "\e[31m red normal \e[0m\n" \
    "\x1B[1;31m red bold \e[0m\n" \
    "\x1B[3;31m red italic \e[0m\n"
}
