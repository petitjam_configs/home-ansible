vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
	pattern = "*.md",
	callback = function()
		vim.opt_local.textwidth = 100
		vim.opt_local.colorcolumn = "+1"
	end,
})

-- Syntax mappings
vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
	pattern = "*gitconfig",
	command = "set syntax=gitconfig",
})
